## Variants Selector Vue Component for selecting sibling variants depend of specification params

#### Install
Add to your package.json in dependencies
``` bash
"vue-plugin-variants-selector": "git+ssh://git@bitbucket.org/digitalholding/vue-plugin-variants-selector.git#{version}"
```
Install packages
``` bash
npm install
```

Include in project globally as plugin
``` bash
import Vue from 'vue';
import VuePluginVariantsSelector from 'vue-plugin-variants-selector';

Vue.use(VuePluginVariantsSelector);
```

Usage
``` bash
<vue-plugin-variants-selector :product="{ object }" :current-variant-code="{ string }" />
```

#### Building & editing:
You can rapidly prototype with just a single *.vue file with the vue serve and vue buildcommands, 
but they require an additional global addon to be installed first:
``` bash
npm install -g @vue/cli
npm install -g @vue/cli-service-global
```

Install packages
``` bash
npm install
```

Serve on localhost command
``` bash
yarn dev
```

Bundling Your Library (need devDependencies: bili, rollup-plugin-vue, vue-template-compiler, node-sass, sass-loader)
``` bash
npx bili --bundle-node-modules
```
