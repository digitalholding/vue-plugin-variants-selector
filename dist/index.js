/*!
 * vue-plugin-variants-selector v0.0.1
 * (c) Dominik Łyczko
 * Released under the MIT License.
 */
'use strict';

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var script = {
  name: "VuePluginVariantsSelector",
  props: {
    product: {
      required: true,
      "default": function _default() {
        return console.log('Missing product object.');
      }
    },
    currentVariantCode: {
      type: String,
      required: true,
      "default": function _default() {
        return console.log('Missing current variant code.');
      }
    }
  },
  data: function data() {
    return {
      openParamCode: null
    };
  },
  computed: {
    specificationParams: function specificationParams() {
      var temp = [];

      if (!!this.product && !!this.product.variants) {
        Object.values(this.product.variants).forEach(function (variant) {
          if (!!variant.specification && !!variant.specification.items) {
            variant.specification.items.forEach(function (param) {
              if (!temp.some(function (e) {
                return e.code == param.code;
              })) {
                temp.push({
                  code: param.code,
                  name: param.name
                });
              }
            });
          }
        });
      }

      return temp;
    },
    activeVariant: function activeVariant() {
      var temp = null;

      if (!!this.product && !!this.product.variants && !!this.product.variants[this.currentVariantCode]) {
        temp = this.product.variants[this.currentVariantCode];
      }

      return temp;
    },
    restVariants: function restVariants() {
      var _this = this;

      var variants = [];

      if (!!this.openParamCode) {
        if (!!this.product && !!this.product.variants) {
          variants = Object.values(this.product.variants);

          if (!!this.activeVariant) {
            if (!!this.activeVariant.code) {
              variants = this.removeVariantByCode(variants, this.activeVariant.code);
            }

            if (!!this.activeVariant.specification && !!this.activeVariant.specification.items) {
              variants.forEach(function (variant) {
                if (!!variant.specification && !!variant.specification.items) {
                  _this.activeVariant.specification.items.forEach(function (param) {
                    if (param.code !== _this.openParamCode) {
                      var paramCorrespondingToActiveVariantParam = variant.specification.items.find(function (item) {
                        return item.code === param.code && item.value.code == param.value.code;
                      });

                      if (paramCorrespondingToActiveVariantParam == undefined) {
                        variants = _this.removeVariantByCode(variants, variant.code);
                      }
                    }
                  });
                } else {
                  variants = _this.removeVariantByCode(variants, variant.code);
                }
              });
            }
          }
        }
      }

      return variants;
    }
  },
  methods: {
    removeVariantByCode: function removeVariantByCode(elements, code) {
      var temp = [];

      if (!!elements && !!code) {
        temp = elements;
        var elementIndex = temp.findIndex(function (element) {
          return element.code == code;
        });

        if (elementIndex != '-1') {
          temp.splice(elementIndex, 1);
        }
      }

      return temp;
    },
    closeSelectors: function closeSelectors() {
      this.openParamCode = null;
    },
    toggleSelector: function toggleSelector(paramCode) {
      if (paramCode == this.openParamCode) {
        this.openParamCode = null;
      } else {
        this.openParamCode = paramCode;
      }
    },
    getParamValueOfVariantByCode: function getParamValueOfVariantByCode(variant, paramCode) {
      var param = null;

      if (!!variant && !!variant.specification && !!variant.specification.items && !!paramCode) {
        param = variant.specification.items.find(function (item) {
          return item.code == paramCode;
        });

        if (!!param && !!param.value && !!param.value.value) {
          return param.value.value;
        }
      }
    },
    changeActiveVariant: function changeActiveVariant(code, param) {
      this.toggleSelector(param);
      this.$router.push('/product/' + code);
    }
  }
};

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
    }
    // Vue.extend constructor export interop.
    const options = typeof script === 'function' ? script.options : script;
    // render functions
    if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true;
        // functional template
        if (isFunctionalTemplate) {
            options.functional = true;
        }
    }
    // scopedId
    if (scopeId) {
        options._scopeId = scopeId;
    }
    let hook;
    if (moduleIdentifier) {
        // server build
        hook = function (context) {
            // 2.3 injection
            context =
                context || // cached call
                    (this.$vnode && this.$vnode.ssrContext) || // stateful
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            // 2.2 with runInNewContext: true
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            // inject component styles
            if (style) {
                style.call(this, createInjectorSSR(context));
            }
            // register component module identifier for async chunk inference
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
    }
    else if (style) {
        hook = shadowMode
            ? function (context) {
                style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
            }
            : function (context) {
                style.call(this, createInjector(context));
            };
    }
    if (hook) {
        if (options.functional) {
            // register for functional component in vue file
            const originalRender = options.render;
            options.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        }
        else {
            // inject component registration as beforeCreate hook
            const existing = options.beforeCreate;
            options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
    }
    return script;
}

const isOldIE = typeof navigator !== 'undefined' &&
    /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
function createInjector(context) {
    return (id, style) => addStyle(id, style);
}
let HEAD;
const styles = {};
function addStyle(id, css) {
    const group = isOldIE ? css.media || 'default' : id;
    const style = styles[group] || (styles[group] = { ids: new Set(), styles: [] });
    if (!style.ids.has(id)) {
        style.ids.add(id);
        let code = css.source;
        if (css.map) {
            // https://developer.chrome.com/devtools/docs/javascript-debugging
            // this makes source maps inside style tags work properly in Chrome
            code += '\n/*# sourceURL=' + css.map.sources[0] + ' */';
            // http://stackoverflow.com/a/26603875
            code +=
                '\n/*# sourceMappingURL=data:application/json;base64,' +
                    btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) +
                    ' */';
        }
        if (!style.element) {
            style.element = document.createElement('style');
            style.element.type = 'text/css';
            if (css.media)
                style.element.setAttribute('media', css.media);
            if (HEAD === undefined) {
                HEAD = document.head || document.getElementsByTagName('head')[0];
            }
            HEAD.appendChild(style.element);
        }
        if ('styleSheet' in style.element) {
            style.styles.push(code);
            style.element.styleSheet.cssText = style.styles
                .filter(Boolean)
                .join('\n');
        }
        else {
            const index = style.ids.size - 1;
            const textNode = document.createTextNode(code);
            const nodes = style.element.childNodes;
            if (nodes[index])
                style.element.removeChild(nodes[index]);
            if (nodes.length)
                style.element.insertBefore(textNode, nodes[index]);
            else
                style.element.appendChild(textNode);
        }
    }
}

/* script */
var __vue_script__ = script;
/* template */

var __vue_render__ = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return !!_vm.product && !!_vm.currentVariantCode ? _c('div', {
    directives: [{
      name: "click-outside",
      rawName: "v-click-outside",
      value: _vm.closeSelectors,
      expression: "closeSelectors"
    }],
    staticClass: "variants-selector-container"
  }, _vm._l(_vm.specificationParams, function (param) {
    return _c('div', {
      staticClass: "param-wrapper"
    }, [_c('div', {
      staticClass: "pseudo-selector",
      "class": {
        open: param.code == _vm.openParamCode,
        scroll: _vm.restVariants.length > 4
      },
      on: {
        "click": function click($event) {
          $event.stopPropagation();
          return _vm.toggleSelector(param.code);
        }
      }
    }, [_c('span', {
      staticClass: "param-name"
    }, [_vm._v(_vm._s(param.name))]), _vm._v(" "), _c('span', {
      staticClass: "active-element-param"
    }, [_vm._v(_vm._s(_vm.getParamValueOfVariantByCode(_vm.activeVariant, param.code)))]), _vm._v(" "), _c('transition-group', {
      staticClass: "list",
      attrs: {
        "name": "fadeanim",
        "mode": "out-in",
        "tag": "div"
      }
    }, [_vm.restVariants.length == 0 && param.code == _vm.openParamCode ? _c('div', {
      key: "0",
      staticClass: "element"
    }, [_vm._v("\n          Brak produktów o innej wartości parametru\n        ")]) : _vm._l(_vm.restVariants, function (variant) {
      return _c('div', {
        key: variant.code,
        staticClass: "element",
        on: {
          "click": function click($event) {
            return _vm.changeActiveVariant(variant.code, param.code);
          }
        }
      }, [_vm._v("\n          " + _vm._s(_vm.getParamValueOfVariantByCode(variant, param.code)) + "\n        ")]);
    })], 2)], 1)]);
  }), 0) : _vm._e();
};

var __vue_staticRenderFns__ = [];
/* style */

var __vue_inject_styles__ = function __vue_inject_styles__(inject) {
  if (!inject) return;
  inject("data-v-3bc1b869_0", {
    source: ".fadeanim-enter-active[data-v-3bc1b869],.fadeanim-leave-active[data-v-3bc1b869]{transition:opacity .3s}.fadeanim-enter[data-v-3bc1b869],.fadeanim-leave-to[data-v-3bc1b869]{opacity:0}.variants-selector-container[data-v-3bc1b869]{width:100%}.variants-selector-container .param-wrapper h3[data-v-3bc1b869]{margin-bottom:10px}.variants-selector-container .param-wrapper .pseudo-selector[data-v-3bc1b869]{display:flex;align-items:center;justify-content:space-between;position:relative;border:1px solid rgba(41,127,202,.1);border-radius:2px;padding:15px 20px;width:100%;margin-bottom:15px;cursor:pointer;transition:border .3s;user-select:none}.variants-selector-container .param-wrapper .pseudo-selector[data-v-3bc1b869]:hover{border:1px solid rgba(41,127,202,.4)}.variants-selector-container .param-wrapper .pseudo-selector[data-v-3bc1b869]:before{content:\"\";border-left:2px solid #183f5c;border-bottom:2px solid #183f5c;width:8px;height:8px;position:absolute;right:15px;top:20px;transform:rotate(-45deg);transition:transform .3s,top .3s}.variants-selector-container .param-wrapper .pseudo-selector.open[data-v-3bc1b869]{border:1px solid rgba(41,127,202,.4);z-index:101}.variants-selector-container .param-wrapper .pseudo-selector.open[data-v-3bc1b869]:before{top:24px;transform:rotate(135deg)}.variants-selector-container .param-wrapper .pseudo-selector.open .list[data-v-3bc1b869]{opacity:1}.variants-selector-container .param-wrapper .pseudo-selector.open .list .element[data-v-3bc1b869]{height:49px;opacity:1}.variants-selector-container .param-wrapper .pseudo-selector.scroll .list[data-v-3bc1b869]{overflow-y:scroll!important}.variants-selector-container .param-wrapper .pseudo-selector .param-name[data-v-3bc1b869]{font-size:1.3rem;letter-spacing:.46px}.variants-selector-container .param-wrapper .pseudo-selector .active-element-param[data-v-3bc1b869]{font-size:1.3rem;margin-right:18px;letter-spacing:.46px}.variants-selector-container .param-wrapper .pseudo-selector .list[data-v-3bc1b869]{position:absolute;top:calc(100% + 1px);left:-1px;background:#fff;width:calc(100% + 2px);z-index:100;opacity:0;transition:opacity .3s;border-left:1px solid rgba(41,127,202,.4);border-bottom:1px solid rgba(41,127,202,.4);border-right:1px solid rgba(41,127,202,.4);max-height:216px!important;overflow-y:hidden;padding-bottom:20px}.variants-selector-container .param-wrapper .pseudo-selector .list .element[data-v-3bc1b869]{display:flex;justify-content:flex-end;align-items:center;position:relative;width:100%;padding:0 38px 0 20px;height:0;font-size:1.3rem;letter-spacing:.46px;transition:height .3s,opacity .3s,color .3s;opacity:0}.variants-selector-container .param-wrapper .pseudo-selector .list .element[data-v-3bc1b869]:before{content:\"\";position:absolute;bottom:1px;right:0;width:calc(100% - 20px);height:1px;background:rgba(41,127,202,.4);transition:background .3s}.variants-selector-container .param-wrapper .pseudo-selector .list .element[data-v-3bc1b869]:hover{color:#0073b0}.variants-selector-container .param-wrapper .pseudo-selector .list .element[data-v-3bc1b869]:hover:before{background:rgba(41,127,202,.8)}@media (max-width:450px){.variants-selector-container .param-wrapper .pseudo-selector[data-v-3bc1b869]{padding:15px}.variants-selector-container .param-wrapper .pseudo-selector .list .element[data-v-3bc1b869]{padding:7px 33px 7px 20px}.variants-selector-container .param-wrapper .pseudo-selector .list .element[data-v-3bc1b869]:before{width:calc(100% - 15px)}}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


var __vue_scope_id__ = "data-v-3bc1b869";
/* module identifier */

var __vue_module_identifier__ = undefined;
/* functional template */

var __vue_is_functional_template__ = false;
/* style inject SSR */

/* style inject shadow dom */

var __vue_component__ = /*#__PURE__*/normalizeComponent({
  render: __vue_render__,
  staticRenderFns: __vue_staticRenderFns__
}, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, false, createInjector, undefined, undefined);

var index = {
  install: function install(Vue, options) {
    Vue.directive('click-outside', {
      bind: function bind(el, binding, vnode) {
        el.clickOutsideEvent = function (event) {
          if (!(el == event.target || el.contains(event.target))) {
            vnode.context[binding.expression](event);
          }
        };

        document.body.addEventListener('click', el.clickOutsideEvent);
      },
      unbind: function unbind(el) {
        document.body.removeEventListener('click', el.clickOutsideEvent);
      }
    });
    Vue.component("vue-plugin-variants-selector", __vue_component__);
  }
};

module.exports = index;
